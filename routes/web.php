<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guard('web')->check()) {
        return redirect()->action('QuestionController@index');
    } else {
        return view('welcome');//landing
    }
});

Auth::routes();

// List/Search all questions
Route::get('/questions{search?}', 'QuestionController@index');
// Filter Questions
Route::get('/questions/filter', 'QuestionController@filter');
//Create Question
Route::post('/questions', 'QuestionController@save');

// List/Search All Games
Route::get('/games', 'GameController@index')->middleware('premium');
// Create&Play a new game
Route::get('/game', 'UserController@createGame');
// Record answer
Route::post('/userstats', 'UserStatsController@save');
// User Profile 
Route::get('/profile/{id}', 'UserController@show');
Route::get('/my-profile', 'UserController@profile')->name('profile');
Route::post('/profile/{id}', 'UserController@update');
