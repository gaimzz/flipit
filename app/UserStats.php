<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStats extends Model
{
    protected $table = "user_stats";
    protected $fillable = ['user_id', 'q_created', 'g_created', 'q_answered', 'q_correct'];
    protected $primaryKey = 'user_id';
    
    public function user()
    {
        return $this->hasOne('App\User');
    }
}
