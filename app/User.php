<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'questions_created', 'games_created', 'premium'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function achievements()
    {
        return $this->belongsToMany('App\Achievement')->withTimestamps();
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function games()
    {
        return $this->hasMany('App\Game');
    }

    public function stats()
    {
        return $this->hasOne('App\UserStats');
    }

}
