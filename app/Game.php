<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    //
    protected $fillable = ['user_id', 'titol', 'type', 'game', 'votes'];
    public function tags()
    {
        return $this->hasMany('App\GameTag');
    }
}
