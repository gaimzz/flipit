<?php

namespace App\Observers;

use App\UserStats;
use App\User;

class UserStatsObserver
{

    public function questionCreator($questions_created)
    {
        if ($questions_created >= 100) :
            return 3;
        elseif ($questions_created >= 30) :
            return 2;
        elseif ($questions_created >= 5) :
            return 1;
        else:
            return 0;
        endif;
    }

    public function gameCreator($games_created)
    {
        if ($games_created >= 100) :
            return 6;
        elseif ($games_created >= 30) :
            return 5;
        elseif ($games_created >= 5) :
            return 4;
        else: 
            return 0;
        endif;
    }

    public function creatorAchievements($user, $user_stats)
    {
        $achievements = array();
        $games_created_achievement = $this->gameCreator($user_stats->g_created);
        if ($games_created_achievement) :
            $achievements[] = $games_created_achievement;
        endif;
        $questions_created_achievement = $this->questionCreator($user_stats->q_created);
        if ($questions_created_achievement) :
            $achievements[] = $questions_created_achievement;
        endif;
        $user->achievements()->sync($achievements);
    }
    
    // public function questionMastery($user, $questions_answered, $questions_correct)
    // {
    //     if ($questions_correct / $questions_answered * 100 > 70) :
    //         $user->achievements()->attach(3);
    //     endif;
    // }
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\UserStats $user_stats
     * @return void
     */
    public function updated(UserStats $user_stats)
    {
        // Check Logros
        //      Si aconsegueix algun -> INSERT INTO achievements_user
        $user = User::find($user_stats->user_id);
        $this->creatorAchievements($user, $user_stats);
        // $this->questionMastery($user, $user_stats->q_answered, $user_stats->q_correct);
    }
}