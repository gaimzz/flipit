<?php

namespace App\Observers;

use App\Question;
use App\UserStats;

class QuestionObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Question  $question
     * @return void
     */
    public function created(Question $question)
    {
        // $question->
        // Crear instancia model UserStats augmenta +1 el created_questions
        $user_stats = UserStats::find($question->user_id);
        $user_stats->q_created++;
        $user_stats->save();
    }
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Question  $question
     * @return void
     */
    public function deleting(Question $question)
    {
        // Crear instancia model UserStats disminuir -1 el created_questions
        $user_stats = UserStats::find($question->user_id);
        $user_stats->q_created--;
        $user_stats->save();
    }
}