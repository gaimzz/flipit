<?php

namespace App\Observers;

use App\Game;
use App\UserStats;

class GameObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Game  $game
     * @return void
     */
    public function created(Game $game)
    {
        $user_stats = UserStats::find($game->user_id);
        $user_stats->g_created++;
        $user_stats->save();
    }
    /**
     * Listen to the User deleting event.
     *
     * @param  \App\Game  $game
     * @return void
     */
    public function deleted(Game $game)
    {
        $user_stats = UserStats::find($game->user_id);
        $user_stats->g_created--;
        $user_stats->save();
    }
}