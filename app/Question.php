<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = "questions";
    protected $fillable = ['question', 'votes', 'language_id', 'user_id'];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function tags()
    {
        return $this->hasMany('App\QuestionTag');
    }
    
    public function language()
    {
        return $this->hasOne('App\Language');
    }
}
