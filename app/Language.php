<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    protected $table = "languages";
    protected $fillable = ['lang_code'];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
