<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Question;
use Illuminate\Support\Facades\DB;
use Auth;

class QuestionController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request->input() == null) :
            $questions = Question::with('answers')->paginate(15);
            return view('questions', compact('questions'));
        else :
            $questions = Question::with('answers')
                            ->join('question_tag', 'questions.id', '=', 'question_tag.question_id')
                            ->select('questions.*')
                            ->where('questions.question', 'LIKE', '%'.$request->search.'%')
                            ->orWhere('question_tag.tag', 'LIKE', '%'.$request->search.'%')
                            ->paginate(15)
                            ->withPath('?search='.$request->search);
            return view('questions', compact('questions'));
        endif;
    }

    public function filter(Request $request)
    {
        $request->validate([
            'orderby' => 'required|in:asc,desc',
            'lang' => 'required|in:0,1,2,3'
        ]);

        $operator = ($request->lang == 0) ? '!=' : '=';
        
        $questions = Question::with('answers')
                              ->where('language_id', $operator, $request->lang)
                              ->orderBy('updated_at', $request->orderby)
                              ->paginate(15)
                              ->withPath('?orderby='.$request->orderby.'&lang='.$request->lang);

        // Remember filter conditions
        $filters = ['lang' => $request->lang, 'updated' => $request->orderby];
        return view('questions', compact('questions', 'filters'));
    }

    public function save(Request $request)
    {
        $request->validate([
            'contingut' => 'required',
            'answers' => 'required'
        ]);
        if ( $request->a1 == 0 && $request->a2 && $request->a3 == 0 && $request->a4 == 0) :
            return redirect('error');
        endif;

        $question = new Question;
        $question->question = $request->contingut;
        $question->user_id = Auth::id();
        $question->language_id = $request->language;
        $question->votes = 0;
        $question->save();
        
        $tags = explode(',', $request->tags);

        foreach ($tags as $tagText) :
            $tag = new \App\QuestionTag(['tag' => $tagText]);
            $question->tags()->save($tag);
        endforeach;

        for($i = 1; $i <= 4; $i++) :
            $bool = (int) $request->input($i);
            $answer = new \App\Answer(['answer' => $request->answers[$i], 'correct' => $bool]);
            $question->answers()->save($answer);
        endfor;

        return redirect()->route('profile');
    }

}
