<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;

class GameController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('premium');
    }
    
    public function index(Request $request)
    {
        if ($request->input() == null) :
            $games = Game::paginate(15);
            return view('games', compact('games'));
        else :
            $games = Game::with('tags')
                        ->join('game_tag', 'games.id', '=', 'game_tag.game_id')
                        ->select('games.*')
                        ->where('games.titol', 'LIKE', '%'.$request->search.'%')
                        ->orWhere('game_tag.tag', 'LIKE', '%'.$request->search.'%')
                        ->paginate(15)
                        ->withPath('?search='.$request->search);
            return view('games', compact('games'));
        endif;
    }
}
