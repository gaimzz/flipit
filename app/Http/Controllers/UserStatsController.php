<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserStats;
use Auth;

class UserStatsController extends Controller
{
    public function save(Request $request)
    {
        $request->validate([
            'correct' => 'required'
        ]);
        $user = UserStats::find(Auth::id());
        if ($request->correct == 'yes') :
            
            $user->q_answered++;
            $user->q_correct++;
            $user->save();
        else :
            $user->q_answered++;
            $user->save();
        endif;
    }
}
