<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Question;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('profile', compact('user'));
    }

    public function profile()
    {
        $user = Auth::user();
        return view('my-profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'bail|required|max:12|unique:users,name,'.$id,
            'email' => 'required|unique:users,email,'.$id
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect()->route('profile');
    }

    public function createGame(Request $request)
    {
        $request->validate([
            'type' => 'required'
        ]);
        $user = Auth::user();
        $questions = Question::with('answers')->get();
        if ($request->type == 'hot-potato') :
            return view('hot-potato', compact('user', 'questions'));
        elseif ($request->type == 'team-race') :
            return view('team-race', compact('user', 'questions'));
        endif;
    }
}
