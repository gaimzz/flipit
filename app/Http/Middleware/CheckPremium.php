<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckPremium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->premium == 0) {
            return redirect('/questions');
        }

        return $next($request);
    }
}
