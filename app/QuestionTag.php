<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionTag extends Model
{
    //
    protected $table = "question_tag";
    protected $fillable = ['question_id', 'tag'];

    public function question()
    {
        return $this->hasOne('App\Question');
    }
}
