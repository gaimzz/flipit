<?php

namespace App\Providers;

use App\Question;
use App\Game;
use App\UserStats;
use App\User;
use App\Observers\QuestionObserver;
use App\Observers\GameObserver;
use App\Observers\UserStatsObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // Eloquent Event Observers
        Question::observe(QuestionObserver::class);
        Game::observe(GameObserver::class);
        UserStats::observe(UserStatsObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
