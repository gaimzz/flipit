@extends('layouts.app')

@section('title', 'Games')
@section('a-games', 'active')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="accordion" id="accordionGames">
                    @foreach($games as $game)
                        <div class="card mb-0">
                            <div class="card-header text-center question-header" id="heading{{$game->id}}">
                                <h5 class="text-center" data-toggle="collapse" data-target="#{{$game->id}}" aria-expanded="true" aria-controls="{{ $game->id }}">
                                        {{ $game->titol }}
                                </h5>
                                @foreach($game->tags as $tag)
                                    <span class="tagshow">{{ $tag->tag }}</span>
                                @endforeach
                            </div>
                            <div id="{{ $game->id }}" class="collapse" aria-labelledby="heading{{$game->id}}" data-parent="accordionGames">
                                <div class="card-body">
                                    <form class="gameForm">
                                        <div class="row justify-content-md-center">
                                            {{ $game->type }}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-2">
                {{ $games->links() }}
            </div>
        </div>
    </div>
@endsection