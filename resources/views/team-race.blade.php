@extends('layouts.app-teamrace')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 m-top-60">
            <h3>Please Select 21 Questions</h3>
            <h5>Questions</h5>
        </div>
        <div class="col-md-12 max-height">
                <div class="accordion" id="accordionQuestions">
                    @foreach($questions as $question)
                        <div class="card mb-0">
                            <div class="card-header text-center question-header" id="heading{{$question->id}}">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{ $question->id }}">
                                </div>
                                <div data-toggle="collapse" data-target="#{{$question->id}}" aria-expanded="true" aria-controls="{{ $question->id }}">
                                    <h5 class="question-{{$question->id}}">{{ $question->question }}</h5>
                                    @foreach($question->tags as $tag)
                                        <span class="tagshow">{{ $tag->tag }}</span>
                                     @endforeach
                                </div>
                            </div>
                            <div id="{{ $question->id }}" class="collapse" aria-labelledby="heading{{$question->id}}" data-parent="accordionQuestions">
                                <div class="card-body">
                                    <form class="questionForm">
                                        <div class="row justify-content-md-center">
                                            @foreach($question->answers as $i => $answer)
                                            <div class="col-md-7 pl-2">
                                                <div class="form-check custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input answer-correct{{$question->id}}{{$i}}" type="radio" name="answer" id="answer{{$answer->id}}" value="{{ $answer->correct }}">
                                                    <label class="custom-control-label answer-{{$question->id}}{{$i}}" for="answer{{$answer->id}}">{{ $answer->answer }}</label>
                                                </div>
                                            </div>
                                            @endforeach
                                            <div class="col-md-12 text-center">
                                                <button id="checkAnswer" class="btn btn-primary">Check Answer</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
        <div class="col-md-12 justify-content-md-center m-top-60 text-center">
            <button id="play" class="btn btn-success">START PLAYING!!</button>
        </div>
    </div>
</div>
@endsection