<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Flip IT - @yield('title')</title>


    <!-- Scripts -->
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/popper.min.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/light-bootstrap-dashboard.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/jquery.tagsinput.js') }}" type="text/javascript" defer></script>
    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/light-bootstrap-dashboard.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.tagsinput.css') }}" >

</head>
<body>
<div class="wrapper">
        <div class="sidebar">
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="http://www.flipit-learning.com" class="simple-text">
                        FlipIt
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item @yield('a-profile')">
                        <a class="nav-link" href="/my-profile">
                            <img class="align-middle menuIcon" src="img/utils/profile.svg"/>
                            My Profile <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target=".bd-example-modal-lg">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Question
                        </a>
                    </li>
                    <li class="nav-item @yield('a-create-game')">
                        <a class="nav-link" data-toggle="modal" data-target="#createGame">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Game
                        </a>
                    </li>
                    <li class="nav-item @yield('a-questions')">
                        <a class="nav-link" href="/questions">
                            <img class="align-middle menuIcon" src="img/utils/questions.svg"/>
                            Questions <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    @if(Auth::user()->premium == 1)
                    <li class="nav-item @yield('a-games')">
                        <a class="nav-link" href="/games">
                            <img class="align-middle menuIcon" src="img/utils/games.svg"/>
                            Games
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <img class="align-middle menuIcon" src="img/utils/logout.svg"/>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar sticky-top navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand mr-2" href="#pablo">@yield('title')</a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navigation">
                        <ul class="nav navbar-nav mr-auto flex-grow-1">
                            <form id="mainSearch" method="GET" action="" class="flex-grow-1">
                                <div class="form-row">
                                    <div class="col-10">
                                        <input name="search" type="text" class="form-control" placeholder="Search" required>
                                    </div>
                                    <div class="col input-group">
                                        <select id="routeSelector" class="form-control custom-select">
                                            <option value='/questions' selected>Questions</option>
                                            @if(Auth::user()->premium == 1)
                                            <option value='/games'>Games</option>
                                            @endif
                                        </select>
                                        <div class="input-group-append">
                                          <button id="xD" class="btn btn-primary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                @yield('content')
            </div>
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Question</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="createQuestion" method="post" action="/questions">
                                @csrf
                                <div class="form-group">
                                    <label for="tags" class="col-form-label">Tags</label>
                                    <input name="tags" id="tags" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Question Language</label>
                                    <select name="language" class="form-control" id="exampleFormControlSelect1" required>
                                        <option value="1">English</option>
                                        <option value="2">Spanish</option>
                                        <option value="3">French</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="contingut" class="col-form-label">Question</label>
                                    <textarea class="form-control" id="contingut" name="contingut"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 1</label>
                                    <textarea class="form-control" id="answer1" name="answers[1]" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 2</label>
                                    <textarea class="form-control" id="answer1" name="answers[2]" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 3</label>
                                    <textarea class="form-control" id="answer1" name="answers[3]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 4</label>
                                    <textarea class="form-control" id="answer1" name="answers[4]"></textarea>
                                </div>
                                <h5 class="text-center"><strong>Select which answers will be CORRECT(min: 1)</strong></h5>
                                <div class="row justify-content-md-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="1">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox1" name="1" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox1">1</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="2">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox2" name="2" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox2">2</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="3">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox3" name="3" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox3">3</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="4">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox4" name="4" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox4">4</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit" form="createQuestion">Save Question</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="createGame" tabindex="-1" role="dialog" aria-labelledby="createGameLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="createGameLabel">Create Game</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                           </button>
                       </div>
                       <div class="modal-body">
                           <form id="gameType" method="GET" action="/game">
                               <h5>Choose game type</h5>
                               <div class="col">
                                       <select name="type" id="gameSelector" class="form-control">
                                           <option value='team-race' selected>Team Race</option>
                                           <option value='hot-potato'>Hot Potato</option>
                                       </select>
                                   </div>
                           </form>
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button form="gameType" type="submit" class="btn btn-primary">Create Game</button>
                       </div>
                   </div>
               </div>
           </div>
        </div>
    </div>
    <!-- BODY POL -->
        <!-- <nav class="navbar navbar-dark bg-dark p-0 d-flex justify-content-around">
            <a class="navbar-brand p-2 d-none d-sm-block" href="#">Never expand</a>
            <form id="mainSearch" class="flex-grow-1 m-top-1 p-2">
                    <div class="form-row">
                      <div class="col-10">
                        <input name="search" type="text" class="form-control" placeholder="Search">
                      </div>
                      <div class="col">
                            <select id="routeSelector" name="route" class="form-control">
                                    <option value='/questions/' selected>Questions</option>
                                    <option value='/games/'>Games</option>
                                    <option value='/users/'>Users</option>
                            </select>
                      </div>
                      <div class="col">
                          <button class="btn btn-primary" type="submit" name="submit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></button>
                      </div>
                    </div>
            </form>
            <div class="dropdown show p-2">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Username
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="/profile/{{ Auth::id() }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    Profile <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/questions">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    Questions <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                                    Create Game
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/games">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                                    Games
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                    @yield('content')
                </main>
            </div>
        </div> -->
</body>
</html>
