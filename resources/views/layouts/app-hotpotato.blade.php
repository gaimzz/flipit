<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Flip IT - @yield('title')</title>


    <!-- Scripts -->
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
    <script id="popper" src="{{ asset('js/popper.min.js') }}" type="text/javascript" defer></script>
    <script id="lightjs" src="{{ asset('js/light-bootstrap-dashboard.js') }}" type="text/javascript" defer></script>
    <script id="tagsjs" src="{{ asset('js/jquery.tagsinput.js') }}" type="text/javascript" defer></script>
    <script id="ckeditor" src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js" defer></script>
    <script src="{{ asset('js/hotpotato.js') }}" type="text/javascript" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Styles -->
    <link id="light" rel="stylesheet" type="text/css" href="{{ asset('css/light-bootstrap-dashboard.css') }}" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link id="custom" rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link id="tags" rel="stylesheet" type="text/css" href="{{ asset('css/jquery.tagsinput.css') }}" >
    <link rel="stylesheet" type="text/css" href="../gameresources/hotpotatoe/css/style.css">
	<link rel="stylesheet" type="text/css" href="../gameresources/hotpotatoe/css/animate.css">

</head>
<body>
<div class="wrapper">
        <div class="sidebar">
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="http://www.flipit-learning.com" class="simple-text">
                        FlipIt
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item @yield('a-profile')">
                        <a class="nav-link" href="/my-profile">
                            <img class="align-middle menuIcon" src="img/utils/profile.svg"/>
                            My Profile <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target=".bd-example-modal-lg">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Question
                        </a>
                    </li>
                    <li class="nav-item @yield('a-create-game')">
                        <a class="nav-link" data-toggle="modal" data-target="#createGame">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Game
                        </a>
                    </li>
                    <li class="nav-item @yield('a-questions')">
                        <a class="nav-link" href="/questions">
                            <img class="align-middle menuIcon" src="img/utils/questions.svg"/>
                            Questions <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    @if(Auth::user()->premium == 1)
                    <li class="nav-item @yield('a-games')">
                        <a class="nav-link" href="/games">
                            <img class="align-middle menuIcon" src="img/utils/games.svg"/>
                            Games
                        </a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <img class="align-middle menuIcon" src="img/utils/logout.svg"/>
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar sticky-top navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand mr-2" href="#pablo">@yield('title')</a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navigation">
                        <ul class="nav navbar-nav mr-auto flex-grow-1">
                            <form id="mainSearch" method="GET" action="" class="flex-grow-1">
                                <div class="form-row">
                                    <div class="col-10">
                                        <input name="search" type="text" class="form-control" placeholder="Search" required>
                                    </div>
                                    <div class="col input-group">
                                        <select id="routeSelector" class="form-control custom-select">
                                            <option value='/questions' selected>Questions</option>
                                            @if(Auth::user()->premium == 1)
                                            <option value='/games'>Games</option>
                                            @endif
                                        </select>
                                        <div class="input-group-append">
                                          <button id="xD" class="btn btn-primary" type="submit">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                @yield('content')
            </div>
            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New Question</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="createQuestion" method="post" action="/questions">
                                @csrf
                                <div class="form-group">
                                    <label for="tags" class="col-form-label">Tags</label>
                                    <input name="tags" id="tags" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Select Question Language</label>
                                    <select name="language" class="form-control" id="exampleFormControlSelect1" required>
                                        <option value="1">English</option>
                                        <option value="2">Spanish</option>
                                        <option value="3">French</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="contingut" class="col-form-label">Question</label>
                                    <textarea class="form-control" id="contingut" name="contingut"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 1</label>
                                    <textarea class="form-control" id="answer1" name="answers[1]" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 2</label>
                                    <textarea class="form-control" id="answer1" name="answers[2]" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 3</label>
                                    <textarea class="form-control" id="answer1" name="answers[3]"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="answer1" class="col-form-label">Answer 4</label>
                                    <textarea class="form-control" id="answer1" name="answers[4]"></textarea>
                                </div>
                                <h5 class="text-center"><strong>Select which answers will be CORRECT(min: 1)</strong></h5>
                                <div class="row justify-content-md-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="1">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox1" name="1" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox1">1</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="2">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox2" name="2" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox2">2</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="3">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox3" name="3" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox3">3</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" value="0" name="4">
                                        <input class="custom-control-input" type="checkbox" id="inlineCheckbox4" name="4" value="1">
                                        <label class="custom-control-label" for="inlineCheckbox4">4</label>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit" form="createQuestion">Save Question</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="createGame" tabindex="-1" role="dialog" aria-labelledby="createGameLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                   <div class="modal-content">
                       <div class="modal-header">
                           <h5 class="modal-title" id="createGameLabel">Create Game</h5>
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                           </button>
                       </div>
                       <div class="modal-body">
                           <form id="gameType" method="GET" action="/game">
                               <h5>Choose game type</h5>
                               <div class="col">
                                       <select name="type" id="gameSelector" class="form-control">
                                           <option value='team-race' selected>Team Race</option>
                                           <option value='hot-potato'>Hot Potato</option>
                                       </select>
                                   </div>
                           </form>
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           <button form="gameType" type="submit" class="btn btn-primary">Create Game</button>
                       </div>
                   </div>
               </div>
           </div>
        </div>
</div>
<div class="play-game" style="display:none">
<nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Flip IT</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExample01">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item @yield('a-profile')">
                        <a class="nav-link" href="/my-profile">
                            <img class="align-middle menuIcon" src="img/utils/profile.svg"/>
                            My Profile <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target=".bd-example-modal-lg">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Question
                        </a>
                    </li>
                    <li class="nav-item @yield('a-create-game')">
                        <a class="nav-link" data-toggle="modal" data-target="#createGame">
                            <img class="align-middle menuIcon" src="img/utils/create.svg"/>
                            Create Game
                        </a>
                    </li>
                    <li class="nav-item @yield('a-questions')">
                        <a class="nav-link" href="/questions">
                            <img class="align-middle menuIcon" src="img/utils/questions.svg"/>
                            Questions <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    @if(Auth::user()->premium == 1)
                    <li class="nav-item @yield('a-games')">
                        <a class="nav-link" href="/games">
                            <img class="align-middle menuIcon" src="img/utils/games.svg"/>
                            Games
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
<div class="container-fluid bg-3 text-center h-100">
		<!-- justify-content-center vertical-center-->
		<div class="row align-items-center h-100" id="game">

			<div class="col-md-5" id="team1">
				<div id="teamName" class="padding border rounded-top questionSection"></div>
				<div class="padding border questionSection" id="questionDiv">
                    
				</div>
				<div class="padding border rounded-bottom questionSection" id="timeDiv"></div>
			</div>

			<div class="col-md-2">
				<svg id="svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
					<g transform="matrix(1.25 0 0 -1.25 0 45)">
						<path id="potatoe" style="fill:#D99E82;" d="M0-118.41C0-35.956,115.19,54.376,207.342-26.258c31.262-27.358,57.595-46.076,57.595-80.633
						c138.228,23.038,157.845-102.427,138.228-161.266c-11.519-34.557-67.594-127.388-276.456-34.557C23.038-256.638,0-168.909,0-118.41
						"/>
						<g id="potatoeShadows">
							<path style="fill:#C1694F;" d="M201.121-139.411c21.119,20.096,41.943,28.864,65.326,32.756c-0.503-0.08-1.003-0.156-1.509-0.241
							c0,9.922-2.173,18.536-6.005,26.426C235.007-89.346,209.403-106.847,201.121-139.411z"/>
							<path style="fill:#C1694F;" d="M126.709-152.967c0,6.358-5.161,11.519-11.519,11.519s-11.519-5.161-11.519-11.519
							c0-6.358,5.161-11.519,11.519-11.519S126.709-159.325,126.709-152.967z"/>
							<path style="fill:#C1694F;" d="M345.571-256.638c-6.37,0-11.519-5.149-11.519-11.519c0-6.37,5.149-11.519,11.519-11.519
							s11.519,5.149,11.519,11.519C357.09-261.787,351.941-256.638,345.571-256.638z"/>
							<path style="fill:#C1694F;" d="M184.304-262.397c0,9.538-7.741,17.279-17.279,17.279c-9.549,0-17.279-7.741-17.279-17.279
							c0-9.538,7.73-17.279,17.279-17.279C176.564-279.676,184.304-271.934,184.304-262.397z"/>
							<path style="fill:#C1694F;" d="M80.633-193.283c0,9.538-7.73,17.279-17.279,17.279s-17.279-7.741-17.279-17.279
							s7.73-17.279,17.279-17.279S80.633-202.82,80.633-193.283z"/>
						</g>
					</g>
				</svg>
			</div>

			<div class="col-md-5" id="team2">
				<div id="teamName" class="padding border rounded-top questionSection"></div>
				<div class="padding border questionSection" id="questionDiv">
				</div>
				<div class="padding border rounded-bottom questionSection" id="timeDiv"></div>
			</div>



			<!-- Modal -->
			<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title"></h5>
						</div>
						<div class="modal-body">
						</div>
						<div class="modal-footer"></div>
					</div>
				</div>
			</div>

			<!-- TURN ALERT -->
			<div id="alert"></div>

		</div>
	</div>
	<div id="fullscreenDiv">
		<img id="fullscreen" src="gameresources/utils/fullscreen.svg" data-toggle="tooltip" data-placement="left" title="Fullscreen" />
	</div>    
</div>
</body>
</html>
