<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Flip IT - @yield('title')</title>
    

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/bootstrap.bundle.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/main.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/popper.min.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/light-bootstrap-dashboard.js') }}" type="text/javascript" defer></script>
    <script src="{{ asset('js/jquery.tagsinput.js') }}" type="text/javascript" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/light-bootstrap-dashboard.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.tagsinput.css') }}" >
    
</head>
<body class="text-center">
    @yield('content')
</body>
</html>
