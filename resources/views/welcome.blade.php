<!doctype html>

<html lang="en">

<head>
  <meta charset="utf-8">

  <title>Flipit</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/style.css?v=1.0">
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <!--script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script-->
  <script src="js/main.js"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Flipit</a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbarResponsive" style="">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#about">About</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#games">Our games</a>
          </li>
          <li class="nav-item">
            <div class="btn-group separador">
              <a href="{{ route('login') }}" type="button" class="btn btn-outline-light">Login</a>
              <a href="{{ route('register') }}" type="button" class="btn btn-outline-light">Register</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>


  <div id="gradient"></div>
  <div id="separador"></div>

  <div class="container text-center" id="content">
    <div class="row">

      <div class="col-lg-8">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <p>Ad qui cupidatat dolor consectetur adipisicing Lorem deserunt culpa aliqua in laboris incididunt reprehenderit esse nostrud. Minim nostrud reprehenderit veniam proident nulla cillum mollit elit laborum excepteur qui officia ut labore sunt.
                Velit ullamco labore quis deserunt velit aute et esse non sit occaecat. Do occaecat exercitation consectetur dolore laborum sit ut cupidatat tempor fugiat est esse velit commodo minim proident nostrud. Non nisi enim deserunt enim reprehenderit
                consequat deserunt duis aliqua nulla aliqua duis occaecat. Et sit cupidatat culpa quis consequat non ut officia veniam minim id proident. Deserunt ipsum tempor amet adipisicing elit deserunt nisi Lorem anim eu irure commodo.</p>
              <button type="button" class="btn btn btn-info">Play</button>
            </div>
            <div class="carousel-item">
              <p>Occaecat reprehenderit non qui deserunt ex ex sit incididunt ut ad officia consectetur reprehenderit elit. Duis ad mollit officia adipisicing eu ipsum cillum fugiat sunt est officia consequat irure. Minim velit sit culpa tempor sunt exercitation
                voluptate. Sint veniam excepteur labore duis duis quis duis commodo eiusmod sint tempor. Tempor cupidatat reprehenderit ea id eiusmod proident dolore culpa sit irure.</p>
              <button type="button" class="btn btn btn-info">Register</button>
            </div>
            <div class="carousel-item">
              <p>Minim sunt magna Lorem nostrud irure ea minim anim dolore ut aliquip anim ipsum. In amet laboris aliqua cupidatat qui consequat duis excepteur voluptate et velit qui proident eiusmod aliqua excepteur. Velit esse quis ut laborum dolor id
                occaecat quis veniam ad adipisicing ullamco laboris commodo officia. Ullamco consectetur sunt pariatur in cupidatat minim deserunt dolore velit nostrud.</p>
              <button type="button" class="btn btn btn-info">Go premium</button>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon carousel-btn" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon carousel-btn" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="col-lg-4" id="monitor">
        <img class="img-fluid" src="img/computer.svg">
      </div>

      <div class="col-lg-12 section" id="games">
        <h1 class="display-4 title">Our Games</h1>
        <div class="card-deck">
          <div class="card">
            <img class="card-img-top img-thumbnail" src="img/games/crosstheriver.jpg" alt="Cross the river game photo.">
            <div class="card-body">
              <h5 class="card-title">Cross the river game</h5>
              <p class="card-text">Enjoy playing this .</p>
            </div>
          </div>
          <div class="card">
            <img class="card-img-top img-thumbnail" src="..." alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Hot potatoe game</h5>
              <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
            </div>
          </div>
          <div class="card">
            <img class="card-img-top img-thumbnail" src="..." alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title">Soon...</h5>
              <p class="card-text">...</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 section" id="about">
        <h1 class="display-4 title">About</h1>
        <p>Et consequat excepteur quis officia proident nulla ut. Cillum ullamco cillum nisi laborum sunt culpa amet veniam ullamco. Reprehenderit id occaecat elit irure fugiat cillum nisi excepteur elit enim eu velit incididunt et aute officia aute. Velit
          id velit occaecat labore elit officia sint officia est magna voluptate. Proident incididunt irure ut elit fugiat tempor velit magna dolore duis esse. Laboris ullamco duis Lorem dolor qui quis sit dolore aliquip consectetur ad eu fugiat.</p>
        <p>Dolor eu nulla culpa dolor labore irure aute incididunt. Ullamco mollit cupidatat esse tempor cupidatat esse magna fugiat aute nulla ad qui cillum excepteur laborum ea. Eiusmod ex cupidatat non est sunt ad fugiat eiusmod cupidatat veniam pariatur
          excepteur duis nisi cupidatat ullamco ullamco. Cupidatat adipisicing quis et excepteur elit anim ipsum nisi aute proident anim deserunt cillum. Cillum nulla sunt pariatur laboris consectetur laborum ad magna sint cillum consequat dolore officia
          ipsum occaecat eu nisi. Aliquip et qui voluptate ad fugiat do veniam duis ad adipisicing voluptate aute velit mollit consequat Lorem nulla.</p>
        <p>Magna laboris id dolore pariatur cillum in minim. Nostrud nostrud id nostrud cillum voluptate veniam dolore aliqua. Excepteur magna reprehenderit eu eiusmod officia ut non ad. Qui id fugiat nostrud pariatur velit ea esse voluptate consequat proident
          ea in deserunt.</p>
        <p>Cupidatat consequat esse deserunt eu aliquip commodo cillum dolor non occaecat quis consequat ea ea eiusmod. Non incididunt quis exercitation irure anim veniam qui in officia aute irure minim veniam est cillum veniam laborum. Aliqua exercitation
          velit consequat tempor reprehenderit labore commodo aute cillum ex minim commodo sint. Quis esse et mollit quis sit incididunt amet cillum do aliquip. Dolore cupidatat dolore consequat id est Lorem eiusmod mollit culpa fugiat sunt irure excepteur
          ipsum.
        </p>
      </div>
    </div>
  </div>
  <div class="section" id="footer">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-12">
          <p>Dolore ea reprehenderit cupidatat fugiat anim ea sit nostrud deserunt ea proident anim fugiat. Aliquip eiusmod non eu occaecat occaecat veniam laboris. Ea incididunt sit esse laborum pariatur reprehenderit fugiat. Ea laboris est nisi voluptate
            duis do sunt magna sunt laboris. Est deserunt aute excepteur officia ex aliqua ex irure Lorem dolore. Nulla duis excepteur sint occaecat aliqua mollit fugiat amet reprehenderit fugiat.</p>
          <p>Aliqua ad reprehenderit aliquip eiusmod cupidatat est ullamco quis voluptate magna velit voluptate consequat ad consectetur aliqua. Deserunt ipsum laboris cillum id dolor mollit laboris. Commodo nostrud nostrud veniam mollit pariatur qui veniam
            culpa culpa exercitation consequat adipisicing aliqua ipsum sit minim aliqua. Qui reprehenderit dolor velit cupidatat voluptate duis ullamco ad culpa laborum amet aliquip amet do exercitation excepteur. Enim velit quis dolor consequat pariatur
            ipsum tempor incididunt mollit. Sunt occaecat pariatur sunt consequat enim duis do ut consectetur aliqua. Nisi consequat cupidatat consectetur non reprehenderit magna exercitation Lorem aute minim est aute consequat magna ad exercitation eiusmod.</p>
          <p>Eu eu irure occaecat eiusmod aliqua ex in enim. Enim deserunt qui velit exercitation esse nostrud minim consectetur duis. Officia nostrud pariatur ipsum velit sit incididunt duis occaecat reprehenderit nulla incididunt exercitation laborum ut
            anim et commodo. Cillum amet ut deserunt ad irure esse qui Lorem eu occaecat amet proident consequat et minim ad. Sunt enim aute eu excepteur ullamco nisi est labore eu id. Labore voluptate eu aute excepteur nisi veniam officia. Minim eu minim
            dolor ad qui sit irure minim non et irure qui nostrud laborum. Veniam voluptate adipisicing non consectetur ut consectetur culpa.</p>
          <p>Aliquip adipisicing ea officia ipsum id elit dolore consectetur incididunt in enim proident consequat deserunt voluptate sunt. Esse eiusmod consequat magna nisi ullamco esse ea aute elit commodo occaecat pariatur do ut excepteur magna. Est commodo
            dolore consectetur tempor nostrud quis elit voluptate consectetur incididunt et dolor commodo. Eiusmod reprehenderit culpa amet exercitation dolore cupidatat amet culpa laboris minim ut est tempor ullamco aliquip. Magna adipisicing ut adipisicing
            laboris commodo irure consectetur qui ex nulla dolore proident velit aliquip sunt. Ipsum magna excepteur do dolore ex enim anim ipsum aliqua tempor nulla esse nisi. Occaecat minim occaecat duis qui cupidatat est nisi laborum cupidatat pariatur
            id consectetur ad aliqua sit. Qui aliqua aute culpa culpa eiusmod sint magna nisi adipisicing exercitation velit cupidatat ipsum et.</p>
        </div>
      </div>
    </div>
    <div id="footer">
</body>
<footer>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</footer>

</html>
