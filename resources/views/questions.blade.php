@extends('layouts.app')

@section('title', 'Questions')
@section('a-questions', 'active')

@section('content')
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-sm-5">
                <form method="GET" action="/questions/filter">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input @if(!empty($filters)) @if($filters['updated'] == 'desc') checked @endif @endif  class="custom-control-input" type="radio" name="orderby" id="inlineRadio1" value="desc">
                                <label class="custom-control-label" for="inlineRadio1">Newest</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input @if(!empty($filters)) @if($filters['updated'] == 'asc') checked @endif @endif class="custom-control-input" type="radio" name="orderby" id="inlineRadio2" value="asc">
                                <label class="custom-control-label" for="inlineRadio2">Oldest</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span for="exampleFormControlSelect1" class="input-group-text">Language</label>
                                </div>
                                    <select name="lang" class="form-control custom-select" id="exampleFormControlSelect1">
                                        <option @if(!empty($filters)) @if($filters['lang'] == 0) checked @endif @endif  value="0">All</option>
                                        <option @if(!empty($filters)) @if($filters['lang'] == 1) checked @endif @endif value="1">English</option>
                                        <option @if(!empty($filters)) @if($filters['lang'] == 2) checked @endif @endif value="2">Spanish</option>
                                        <option @if(!empty($filters)) @if($filters['lang'] == 3) checked @endif @endif value="3">French</option>
                                    </select>
                                    <div class="input-group-append">
                                      <button type="submit" class="btn btn-primary">Filter</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            @if ($errors->any())
                                <div class="col-md-12">
                                    <div class="alert alert-danger ml-2">
                                        @foreach ($errors->all() as $error)
                                            <p><strong>- </strong>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="accordion" id="accordionQuestions">
                    @foreach($questions as $question)
                        <div class="card mb-0">
                            <div class="card-header text-center question-header" id="heading{{$question->id}}">
                                <div data-toggle="collapse" data-target="#{{$question->id}}" aria-expanded="true" aria-controls="{{ $question->id }}">
                                    <h5>
                                        {!! $question->question !!}
                                    </h5>
                                    @foreach($question->tags as $tag)
                                        <span class="tagshow">{{ $tag->tag }}</span>
                                     @endforeach
                                </div>
                            </div>
                            <div id="{{ $question->id }}" class="collapse" aria-labelledby="heading{{$question->id}}" data-parent="accordionQuestions">
                                <div class="card-body">
                                    <form class="questionForm" data-id="{{$question->id}}">
                                        <div class="row justify-content-md-center">
                                            @foreach($question->answers as $answer)
                                            <div class="col-md-7 pl-2">
                                                <div class="form-check custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="answer" id="answer{{$answer->id}}" value="{{ $answer->correct }}">
                                                    <label class="custom-control-label" for="answer{{$answer->id}}">
                                                        {{ $answer->answer }}
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                            <input type="hidden" name="question_id" value="{{$question->id}}">
                                            <div class="col-md-12 text-center">
                                                <div class="correct correct{{$question->id}} alert alert-success">Correct!!! :D</div>
                                                <div class="incorrect incorrect{{$question->id}} alert alert-danger">Incorrect!!! D:</div>
                                                <button id="checkAnswer" class="btn btn-primary">Check Answer</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-2">
                {{ $questions->links() }}
            </div>
        </div>
    </div>
@endsection
