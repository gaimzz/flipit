<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
	<script type="text/javascript" src="gameresources/crosstheriver/js/constants.js"></script>
	<script type="text/javascript" src="gameresources/crosstheriver/js/main2.js"></script>
	<link rel="stylesheet" type="text/css" href="gameresources/crosstheriver/css/style.css">
	<link rel="stylesheet" type="text/css" href="gameresources/crosstheriver/css/animate.css">
</head>
<body>
	<div class="col-md-1 floor shadow" id="start"></div>
	<div class="vertical-center">
		<div class="container-fluid bg-3 text-center">

			<div class="row" id="game">

				<div class="col-md-1 section">
					<div class="column" id="teams">
					</div>
				</div>

				<div class="col-md-11 section" id="questions">
					<div id="column0" class='column'></div>
					<div id="column1" class='column'></div>
					<div id="column2" class='column'></div>
					<div id="column3" class='column'></div>
					<div id="column4" class='column'></div>
					<div id="column5" class='column'></div>
				</div>

				<!-- QUESTION MODAL -->
				<div class="modal fade" id="questionModal" tabindex="-1" role="dialog" aria-labelledby="questionModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="Discard question">
						          <img class="cancel" src='gameresources/utils/cancel.svg'  />
						        </button>
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer"></div>
						</div>
					</div>
				</div>

				<!-- INFO MODAL -->
				<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"></h5>
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer"></div>
						</div>
					</div>
				</div>

				<!-- TURN ALERT -->
				<div id="alert"></div>

			</div>
		</div>
	</div>
	<div class="col-md-2 floor shadow" id="end"></div>
	<div id="fullscreenDiv">
		<img id="fullscreen" src="gameresources/gameresources/utils/fullscreen.svg" data-toggle="tooltip" data-placement="left" title="Pantalla completa" />
	</div>
</body>
</html>
