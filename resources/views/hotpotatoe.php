<html>
<head>
	<meta charset="utf-8">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
	<script type="text/javascript" src="gameresources/hotpotatoe/js/main.js"></script>
	<link rel="stylesheet" type="text/css" href="gameresources/hotpotatoe/css/style.css">
</head>
<body>
	<!--div class="vertical-center"-->
	<div class="container-fluid bg-3 text-center h-100">
		<!-- justify-content-center vertical-center-->
		<div class="row align-items-center h-100" id="game">

			<div class="col-md-5" id="team1">
				<div id="teamName" class="padding border rounded-top questionSection"></div>
				<div class="padding border questionSection" id="questionDiv">
				</div>
				<div class="padding border rounded-bottom questionSection" id="timeDiv"></div>
			</div>

			<div class="col-md-2">
				<svg id="svg" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
					<g transform="matrix(1.25 0 0 -1.25 0 45)">
						<path id="potatoe" style="fill:#D99E82;" d="M0-118.41C0-35.956,115.19,54.376,207.342-26.258c31.262-27.358,57.595-46.076,57.595-80.633
						c138.228,23.038,157.845-102.427,138.228-161.266c-11.519-34.557-67.594-127.388-276.456-34.557C23.038-256.638,0-168.909,0-118.41
						"/>
						<g id="potatoeShadows">
							<path style="fill:#C1694F;" d="M201.121-139.411c21.119,20.096,41.943,28.864,65.326,32.756c-0.503-0.08-1.003-0.156-1.509-0.241
							c0,9.922-2.173,18.536-6.005,26.426C235.007-89.346,209.403-106.847,201.121-139.411z"/>
							<path style="fill:#C1694F;" d="M126.709-152.967c0,6.358-5.161,11.519-11.519,11.519s-11.519-5.161-11.519-11.519
							c0-6.358,5.161-11.519,11.519-11.519S126.709-159.325,126.709-152.967z"/>
							<path style="fill:#C1694F;" d="M345.571-256.638c-6.37,0-11.519-5.149-11.519-11.519c0-6.37,5.149-11.519,11.519-11.519
							s11.519,5.149,11.519,11.519C357.09-261.787,351.941-256.638,345.571-256.638z"/>
							<path style="fill:#C1694F;" d="M184.304-262.397c0,9.538-7.741,17.279-17.279,17.279c-9.549,0-17.279-7.741-17.279-17.279
							c0-9.538,7.73-17.279,17.279-17.279C176.564-279.676,184.304-271.934,184.304-262.397z"/>
							<path style="fill:#C1694F;" d="M80.633-193.283c0,9.538-7.73,17.279-17.279,17.279s-17.279-7.741-17.279-17.279
							s7.73-17.279,17.279-17.279S80.633-202.82,80.633-193.283z"/>
						</g>
					</g>
				</svg>
			</div>

			<div class="col-md-5" id="team2">
				<div id="teamName" class="padding border rounded-top questionSection"></div>
				<div class="padding border questionSection" id="questionDiv">
				</div>
				<div class="padding border rounded-bottom questionSection" id="timeDiv"></div>
			</div>



			<!-- Modal -->
			<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title"></h5>
						</div>
						<div class="modal-body">
						</div>
						<div class="modal-footer"></div>
					</div>
				</div>
			</div>

			<!-- TURN ALERT -->
			<div id="alert"></div>

		</div>
	</div>
	<div id="fullscreenDiv">
		<img id="fullscreen" src="gameresources/utils/fullscreen.svg" data-toggle="tooltip" data-placement="left" title="Fullscreen" />
	</div>
	<!--/div-->
</body>
</html>
