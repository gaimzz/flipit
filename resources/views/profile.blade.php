@extends('layouts.app')

@section('title', 'My Profile')
@section('a-profile', 'active')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $user->name }} Profile</h4>
                    </div>
                    <div class="card-body">
                        <p>{{ $user->email }} </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Achievements</h4>
                    </div>
                    <div class="card-body">
                        @foreach($user->achievements as $achievement)
                            <h6>Nom Logro: </h6>
                            {{ $achievement->nom}}
                        @endforeach
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Statistics</h4>
                    </div>
                    <div class="card-body">
                    <h6>Questions Answered</h6>
                    {{ $user->stats->q_answered }}
                    <h6>Question Answered correctly</h6>
                    {{ $user->stats->q_correct }}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h2>My Questions</h2>
                <div class="accordion" id="accordionQuestions">
                    @foreach($user->questions as $question)
                    <div class="card mb-0">
                            <div class="card-header text-center question-header" id="heading{{$question->id}}">
                                <div data-toggle="collapse" data-target="#{{$question->id}}" aria-expanded="true" aria-controls="{{ $question->id }}">
                                    <h5>
                                        {{ $question->question }}
                                    </h5>
                                    @foreach($question->tags as $tag)
                                        <span class="tagshow">{{ $tag->tag }}</span>
                                     @endforeach
                                </div>
                            </div>
                            <div id="{{ $question->id }}" class="collapse" aria-labelledby="heading{{$question->id}}" data-parent="accordionQuestions">
                                <div class="card-body">
                                    <form class="questionForm">
                                        <div class="row justify-content-md-center">
                                            @foreach($question->answers as $answer)
                                            <div class="col-md-7 pl-2">
                                                <div class="form-check custom-control custom-radio custom-control-inline">
                                                    <input class="custom-control-input" type="radio" name="answer" id="answer{{$answer->id}}" value="{{ $answer->correct }}">
                                                    <label class="custom-control-label" for="answer{{$answer->id}}">
                                                        {{ $answer->answer }}
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                            <input type="hidden" name="question_id" value="{{$question->id}}">
                                            <div class="col-md-12 text-center">
                                                <div class="correct correct{{$question->id}} alert alert-success">Correct!!! :D</div>
                                                <div class="incorrect incorrect{{$question->id}} alert alert-danger">Incorrect!!! D:</div>
                                                <button id="checkAnswer" class="btn btn-primary">Check Answer</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @if ($user->premium == 1)
            <div class="col-md-12">
                <h2>My Games</h2>
                    <div class="accordion" id="accordionGames">
                        @foreach($user->games as $game)
                        <div class="card mb-0">
                            <div class="card-header text-center question-header" id="heading{{$game->id}}">
                                <h5 class="text-center" data-toggle="collapse" data-target="#{{$game->id}}" aria-expanded="true" aria-controls="{{ $game->id }}">
                                        {{ $game->titol }}
                                </h5>
                                @foreach($game->tags as $tag)
                                    <span class="tagshow">{{ $tag->tag }}</span>
                                @endforeach
                            </div>
                            <div id="{{ $game->id }}" class="collapse" aria-labelledby="heading{{$game->id}}" data-parent="accordionGames">
                                <div class="card-body">
                                    <form class="gameForm">
                                        <div class="row justify-content-md-center">
                                            {{ $game->type }}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>    
@endsection
