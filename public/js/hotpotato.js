$(document).ready(function() {
    $(document).scroll(function () {
	    var $nav = $(".fixed-top");
	    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
	});
    $('#mainSearch').submit(function(e) {
        $('#mainSearch').attr('action', $('#routeSelector').val());
        $('#mainSearch').submit();
    });
    CKEDITOR.replace('contingut', {
        uiColor: '#ededed'
      });
    $('#tags').tagsInput();
    var QAArray = [];
    $('#play').click(function(){
        let arrayLength = 0;
        var checkedValues = $('.form-check-input:checkbox:checked').map(function() {
            arrayLength = arrayLength + 1;
            let question = {
                question: $('.question-'+this.value).text(),
                answers: [$('.answer-'+this.value+'0').text(), $('.answer-'+this.value+'1').text(), $('.answer-'+this.value+'2').text(), $('.answer-'+this.value+'3').text()],
                time: 20
            }
            for(let i = 0; i < 4; i++) {
                if($('.answer-correct'+this.value+i).val() == 1) {
                    question.correctAnswer = $('.answer-'+this.value+i).text();
                    break;
                }
            }
            QAArray.push(question);
        }).get();
        if(arrayLength < 21) {
            // Sisplau selecciona 21 preguntes per començar el joc
        } else {
            $('.wrapper').css('display', 'none');
            $('#light').remove();
            $('#lightjs').remove();
            $('#popper').remove();
            $('#tagsjs').remove();
            $('#ckeditor').remove();
            $('#custom').remove();
            $('#tags').remove();
            $('.play-game').css('display', 'block');
            initGame();
        }
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
  })
  var turn = 0;
  var time = 50;
  var timeAux = time;
  var i = 0;
  var qCountdownTimer;
  var countdownTimer;
  var qTime;
  var time;
  var questionData;
  var seconds;
  var teamDiv;
  var divisor;
  var teamName;
  var teamImage;
  var K_TEAM1_IMAGE = "gameresources/team/frog_blue.svg"
  var K_TEAM2_IMAGE = "gameresources/team/frog_brown.svg"
  var K_TEAM1_NAME = "Team1Name"
  var K_TEAM2_NAME = "Team2Name"

  function initGame() {
      $(".modal-title").html("Welcome to the potatoe game")
      $(".modal-body").html("<p>Intrucciones:</p>")
      $(".modal-body").append("<p>1. Do your thing</p>")
      $(".modal-body").append("<p>2. Go play</p>")
      $(".modal-footer").html("<button type='button' id='startBtn' class='btn btn-outline-primary' data-dismiss='modal'>Start</button>")
      $("#modal").modal('show')
      $(".modal-footer").on('click', '#startBtn',function() {
        start();
      })
  }

  function endGame() {
      clearInterval(qCountdownTimer);
      $(".modal-title").html("Time over.")
      teamName = turn % 2 != 0 ? K_TEAM1_NAME : K_TEAM2_NAME
      teamImage = turn % 2 != 0 ? K_TEAM1_IMAGE : K_TEAM2_IMAGE
      $(".modal-body").html("<p>The winner is <b>" + teamName + "</b></p>")
      $(".modal-body").append("<p><img class='teamImage' src='" + teamImage + "'></p>")
      $(".modal-footer").html("<button type='button' id='restart' class='btn btn-outline-primary'>Start again</button>")
      $("#modal").modal('show')

      $("#restart").click(function() {
          resetAll()
          $("#questionDiv").html("")
          initGame();
      });
  }

  function resetAll() {
      $("#svg").removeClass()
      $("#potatoe").css("fill", "#D99E82")
      $("#potatoeShadows").children().css("fill", "#C1694F")
      $("#alert").html("")
      $("#team1").children().html("")
      $("#team2").children().html("")
  }

  function start() {
      inicializeTeams()
      turn = 0
      i = 0
      $("#potatoe").css("fill", "#D99E82")
      $("#svg").addClass("animated infinite slow")

      timeAux = time
      seconds = time

      function secondPassed() {
          if (seconds == 0) {
              clearInterval(countdownTimer);
              endGame()
          } else {
              applyMovementClass(seconds)
              seconds--;
          }
      }
      countdownTimer = setInterval(secondPassed, 1000);

      $("#alert").html("<h3>Turn of the team <b>" + K_TEAM1_NAME + "</b></h3>")
      showQuestion()
  }

  function applyMovementClass(second) {
      divisor = time / 5
      if (second <= (timeAux - divisor)) {
          timeAux = timeAux - divisor;
          if (i == 0) {
              $("#svg").addClass("animated infinite medium")
              $("#potatoe").css("fill", "#D98C82")
              $("#potatoeShadows").children().css("fill", "#B45C50")
          } else if (i == 1) {
              $("#svg").addClass("animated infinite fast")
              $("#potatoe").css("fill", "#CA7989")
              $("#potatoeShadows").children().css("fill", "#A74A5C")
          } else if (i == 2) {
              $("#svg").addClass("animated infinite faster")
              $("#potatoe").css("fill", "#BF596F")
              $("#potatoeShadows").children().css("fill", "#9A2F46")
          } else {
              $("#svg").addClass("animated infinite fastest")
              $("#potatoe").css("fill", "#E15357")
              $("#potatoeShadows").children().css("fill", "#CA2E32")
          }
          i++
      }
              console.log("i : " + i)
  }

  function showQuestion() {

          questionData = QAArray[turn]

          teamDiv = turn % 2 == 0 ? "#team1" : "#team2"

          $(teamDiv + " #questionDiv").html("")

          $(teamDiv + " #questionDiv").append("<p><b>" + questionData.question + "</b></p>")
          $.each(questionData.answers, function( index, answer ) {
              $(teamDiv + " #questionDiv").append("<p><button type='button' id='startBtn' class='btn btn-outline-primary question' value='" + answer + "'>" + answer + "</button></p>")
          })

          qTime = questionData.time

          function questionTime() {
              if (qTime == 0) {
                  clearInterval(qCountdownTimer);
                  $(teamDiv + " #timeDiv").html("")
                  $(teamDiv + " #questionDiv").html("<h5>Time over!</h5>")
                  $(teamDiv + " #questionDiv").append("<p><b>Question:</b></p>")
                  $(teamDiv + " #questionDiv").append("<p>" + questionData.question + "</p>")
                  $(teamDiv + " #questionDiv").append("<p><b>Correct answer:</b></p>")
                  $(teamDiv + " #questionDiv").append("<p>" + questionData.correctAnswer + "</p>")

                  nextTurn()

                  showQuestion()
              } else {
                  $(teamDiv + " #timeDiv").html(qTime)
                  qTime--;
              }
          }
          qCountdownTimer = setInterval(questionTime, 1000);

          $(".question").click(function() {
              console.log(this.value);
              if (questionData.correctAnswer == this.value) {
                  clearInterval(qCountdownTimer);
                  $(teamDiv + " #questionDiv").html("<h5>Last question was:</h5>")
                  $(teamDiv + " #questionDiv").append("<p><b>Right answer!</b></p>")
                  $(teamDiv + " #questionDiv").append("<img class='resultImage' src='gameresources/utils/correct.svg'/>")

                  nextTurn()

                  showQuestion()
              } else {
                  $(this).attr("disabled", "true")
                  $(this).removeClass("btn-outline-primary")
                  $(this).addClass("btn-secondary")
                  $(this).append("  <img class='wrongAnswer' src='gameresources/utils/incorrect.svg'/>")
              }

              if (turn > QAArray.length) {
                  console.log("final")
              }
          })
  }

  function nextTurn() {
      turn++
      teamName = turn % 2 == 0 ? K_TEAM1_NAME : K_TEAM2_NAME
      $("#alert").html("<h3 clas='align-middle'>Turn of the team <b>" + teamName + "</b></h3>")
  }

  function inicializeTeams() {
      $("#team1 #teamName").html("<p><img class='teamImage' src='" + K_TEAM1_IMAGE + "'></img></p>")
      $("#team1 #teamName").append("<span>" + K_TEAM1_NAME + "</span>")
      $("#team2 #teamName").html("<p><img class='teamImage' src='" + K_TEAM2_IMAGE + "'></img></p>")
      $("#team2 #teamName").append("<span>" + K_TEAM2_NAME + "</span>")
  }

  $("#fullscreen").click(function() {
      var elem = document.documentElement;
  var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);

  if (isInFullScreen) {
    cancelFullScreen(document);
  } else {
    requestFullScreen(elem);
  }
  return false;
  });

  function cancelFullScreen(el) {
  var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
  if (requestMethod) { // cancel full screen.
    requestMethod.call(el);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
}

function requestFullScreen(el) {
  // Supports most browsers and their versions.
  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;

  if (requestMethod) { // Native full screen.
    requestMethod.call(el);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
  return false
}
});