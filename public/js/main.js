$(document).ready(function() {
    // check answers
    $('.questionForm').submit(function(e) {
        e.preventDefault();
        var form = $(this).serializeArray();
        if(form[0].value == '1') { // Resposta correcte
            recordStats('yes');
            $('.incorrect'+form[1].value).hide();
            $('.correct'+form[1].value).show();
        } else {
            recordStats('no');
            $('.incorrect'+form[1].value).show();
            $('.correct'+form[1].value).hide();
        }
    });
    
    function recordStats(correct) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            url: '/userstats',
            data: {
                correct : correct
            },
            success: function(response) {
                console.log(response)
            },
            error: function(response) {
                console.log(response)
            }
        });
    }
    
    $(document).scroll(function () {
	    var $nav = $(".fixed-top");
	    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
	});
    $('#mainSearch').submit(function(e) {
        $('#mainSearch').attr('action', $('#routeSelector').val());
        $('#mainSearch').submit();
    });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {
        modal_this = this
        $(document).on('focusin.modal', function (e) {
            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')
            && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
                modal_this.$element.focus()
            }
        })
    };
    CKEDITOR.replace('contingut', {
        uiColor: '#ededed'
      });
    $('#tags').tagsInput();
    var QAArray = [];
    $('#play').click(function(){
        let arrayLength = 0;
        var checkedValues = $('.form-check-input:checkbox:checked').map(function() {
            arrayLength = arrayLength + 1;
            let question = {
                question: $('.question-'+this.value).text(),
                answers: [$('.answer-'+this.value+'0').text(), $('.answer-'+this.value+'1').text(), $('.answer-'+this.value+'2').text(), $('.answer-'+this.value+'3').text()],
                time: 20
            }
            for(let i = 0; i < 4; i++) {
                if($('.answer-correct'+this.value+i).val() == 1) {
                    question.correctAnswer = $('.answer-'+this.value+i).text();
                    break;
                }
            }
            QAArray.push(question);
        }).get();
        if(arrayLength < 21) {
            // Sisplau selecciona 21 preguntes per començar el joc
        } else {
            $('.wrapper').css('display', 'none');
            $('#light').remove();
            $('#lightjs').remove();
            $('.play-game').css('display', 'block');
            initGame();
        }
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
  })
  // team race game
  class Team {
      constructor (id, name) {
          this._id = id
          this._name = name
          this._playing = false
          this._x = null
          this._y = null
      }

      set isPlaying (playing) 	{ this._playing = playing }
      get isPlaying ()			{ return this._playing }
      set x (x) 					{ this._x = x }
      get x ()					{ return this._x }
      set y (y) 					{ this._y = y }
      get y ()					{ return this._y }
      get name () 				{ return this._name }
      get id () 					{ return this._id }
  }
  // MODAL INFO
  const K_WELCOME_TITLE = "<b>Welcome to the Cross The River Game!</b>";
  const K_CONTENT= "<p>Answer all the questions to cross to the other side faster than your oponent!</p><p>Teams participating in this game:</p>"

  // BG IMAGES
  //var K_BACKGROUND = "url('gameresources/backgrounds/pool.jpg') repeat";
  //var K_BACKGROUND = "url('gameresources/backgrounds/lake.jpg') repeat";
  //var K_BACKGROUND = "url('gameresources/backgrounds/grass.jpg') repeat";
  var K_BACKGROUND = "url('gameresources/backgrounds/lava.jpg') repeat";

  // FLOOR IMAGES
  //var K_FLOOR = "url('gameresources/floors/wood.jpg') repeat";
  var K_FLOOR = "url('gameresources/floors/lava.jpg') repeat";
  //var K_FLOOR = "url('gameresources/floors/grass.jpg') repeat";

  // TEAM IMAGES
  //var K_TEAM1_IMAGE = "gameresources/team/inflatable_pink.svg";
  //var K_TEAM2_IMAGE = "gameresources/team/inflatable_green.svg";
  //var K_TEAM1_IMAGE = "gameresources/team/frog_blue.svg";
  //var K_TEAM2_IMAGE = "gameresources/team/frog_brown.svg";
  var K_TEAM1_IMAGE = "gameresources/team/charmander_orange.svg";
  var K_TEAM2_IMAGE = "gameresources/team/charmander_purple.svg";
  // QUESTIONS
  //var K_QUESTION_IMAGE = "gameresources/questions/lilypad.svg";
  //var K_QUESTION_IMAGE = "gameresources/questions/float.svg";
  var K_QUESTION_IMAGE = "gameresources/questions/brickwall.svg";

  const K_QUESTION_CORRECT = "Respuesta correcta!";
  const K_QUESTION_INCORRECT = "Respuesta incorrecta :(";
  // COUNTDOWN
  const K_COUNTDOWN_TIMEOUT = "Se acabo el tiempo!";
  // BUTTONS
  const K_BUTTON_CONTINUE = "<button type='button' id='continue' class='btn btn-outline-secondary' data-dismiss='modal'>Continuar</button>";
  const K_BUTTON_START = "<button type='button' id='startBtn' class='btn btn-outline-primary' data-dismiss='modal'>Start</button>";
  // Definicion de variables
  var defaultTimeLeft = 100;
  var questionData;
  var seconds;
  // Instacia dels jugadors
  var team1 = new Team(1, "lolis team")
  var team2 = new Team(2, "team2")
  var actualTeam;
  var enemyTeam;
  var question;
  var index;
  var x;
  var y;
  var countdownTimer;
  var columnNumber = 5;
  var questionsOnColumn = ( 21 - 1 ) / columnNumber;
  var correct = false;

  

  // Carga del modal al iniciar la pagina
  function showInfoModal() {
      $("#infoModal .modal-title").html("");
      $("#infoModal .modal-body").html("");
      $("#infoModal .modal-footer").html("");
      $("#infoModal .modal-title").html(K_WELCOME_TITLE);
      $("#infoModal .modal-body").append(K_CONTENT);
      $("#infoModal .modal-body").append("<img data-toggle='tooltip' data-placement='bottom' title='" + team1.name + "' class='team mx-auto' src='" + K_TEAM1_IMAGE + "'/><span>" + team1.name + "</span>");
      $("#infoModal .modal-body").append("<img data-toggle='tooltip' data-placement='bottom' title='" + team2.name + "' class='team mx-auto' src='" + K_TEAM2_IMAGE + "'/><span>" + team2.name + "</span>");
      $("#infoModal .modal-footer").html(K_BUTTON_START);
      $('#infoModal').modal('show');
  }

  function inicializeQuestions() {
      index = 0;
      // Mostrar preguntes
      for (var x = 0; x < columnNumber; x++) {
          for (var y = 0; y < questionsOnColumn; y++) {
              console.log('xd');
              $("#column" + x ).append("<div x='" + x + "' y='" + y + "' index='" + index + "' data-toggle='modal' data-target='#questionModal' class='question'><img class='questionImage shadow blackAndWhite' src='" + K_QUESTION_IMAGE + "'/></div>");
              index++;
          }
      }
      // Ultima pregunta
      $("#column5").append("<div x='5' y='0' index='" + index++ + "' data-toggle='modal' data-target='#questionModal' class='question'><img class='questionImage shadow blackAndWhite' src='" + K_QUESTION_IMAGE + "'/></div>");
  }

  // Question click event
  $('.column').on('click', '.question', function() {
      // Netejar anterior pregunta
      $(".modal-body").empty();
      $(".modal-footer").empty();

      // Element de la pregunta
      question = this;

      // Posicions i index
      x = parseInt($(question).attr("x"))
      y = parseInt($(question).attr("y"))
      index = parseInt($(question).attr("index"))

      // Recollir les dades de la pregunta
      questionData = QAArray[index];
      // Afegir dades al modal
      $("#questionModal .modal-title").html("<p><b>" + questionData.question + "</b></p>");

      $.each(randsort(questionData.answers), function( index, answer ) {
          $("#questionModal .modal-body").append("<p><button type='button' class='btn btn-outline-primary answer' value='" + answer + "' >" + answer + "</button></p>");
      });

      // Botó cancelar
      $("#questionModal .modal-body").append("<div class='cancel'></div>");

      // Mostrar contador
      seconds = questionData.time
      function secondPassed() {
          $("#questionModal .modal-footer").html(seconds);
          if (seconds == 0) {
              clearInterval(countdownTimer);
              $("#questionModal .modal-body").html(K_COUNTDOWN_TIMEOUT);
              $("#questionModal .modal-footer").html(K_BUTTON_CONTINUE);
              // CUTRE AF
              $("#continue").click(function() {
                  nextTurn();
                  showPossibleQuestions();
              })

          } else {
              seconds--;
          }
      }
      countdownTimer = setInterval(secondPassed, 1000);

      // Contestar la pregunta event
      $(".answer").click(function() {
          clearInterval(countdownTimer);
          //$("#questionModal .modal-body").empty();

          // --------------------------  REPOSTA CORRECTA ---------------------------------
          if (this.value == questionData.correctAnswer) {

              if (x == 5) {
                  $("#questionModal").modal('hide');
                  // Final del joc
                  $("#infoModal .modal-title").html("<b>Game over</b>");
                  $("#infoModal .modal-body").html("<p>The team <b>" + actualTeam.name + "</b> wins this game!</p>");
                  $("#infoModal .modal-body").append("<p>Congratulations!</p>");
                  $("#infoModal .modal-body").append("<img data-toggle='tooltip' data-placement='bottom' title='" + team1.name + "' class='team mx-auto' src='" + K_TEAM1_IMAGE + "'/><span>" + team1.name + "</span>");
                  $("#infoModal .modal-footer").append("<button type='button' id='restart' class='btn btn-outline-secondary' data-dismiss='modal'>Restart</button>");
                  $("#infoModal").modal('show');

                  $("#restart").click(function() {
                      restartGame()
                  })
              } else {
                  $("#questionModal .modal-body").html(K_QUESTION_CORRECT);
                  correct = true;
              }
              // FIXME
              $(question).attr("answered", "true")
          } else {
              $("#questionModal .modal-body").html(K_QUESTION_INCORRECT);
              correct = false;
          }
          $("#questionModal .modal-footer").html(K_BUTTON_CONTINUE);

          $("#continue").click(function() {
              if (correct) { moveActualTeam(x, y) }
              nextTurn();
              showPossibleQuestions();
          })
      });
  });


  function moveActualTeam(x, y) {
      if (actualTeam.x == null) {
          $("#team" + actualTeam.id).addClass("animated fadeOut")
      } else {
          $("[x='"+ actualTeam.x +"'][y='"+actualTeam.y+"']").addClass('animated fadeOut')
      }

      actualTeam.x = x;
      actualTeam.y = y;


      if (actualTeam.id == 1) {
          $(".question").filter("[x='" + x + "'][y='" + y + "']").append("<img data-toggle='tooltip' id='team1' data-placement='left' title='" + team1.name + "' class='teamMove animated fadeIn shadow' src='" + K_TEAM1_IMAGE + "' id='team1'/>")
      } else {
          $(".question").filter("[x='" + x + "'][y='" + y + "']").append("<img data-toggle='tooltip' id='team2' data-placement='left' title='" + team2.name + "' class='teamMove animated fadeIn shadow' src='" + K_TEAM2_IMAGE + "' id='team1'/>")
      }

  }

  function inicializeTeams() {
      $("#teams").append("<img data-toggle='tooltip' id='team1' data-placement='left' title='" + team1.name + "' class='team shadow' src='" + K_TEAM1_IMAGE + "'></img>")
      $("#teams").append("<img data-toggle='tooltip' id='team2' data-placement='left' title='" + team2.name + "' class='team shadow' src='" + K_TEAM2_IMAGE + "'></img>")
  }

  function initGame() {
      inicializeBackground();
      showInfoModal();
      inicializeTeams();
      inicializeQuestions();

      $("#startBtn").click(function() {
          let gameOver = false;
          team1.isPlaying = true;
          actualTeam = team1;
          enemyTeam = team2;
          $("#alert").html("<h3>Turn of the team <b>" + actualTeam.name + "</b></h3>")

          showPossibleQuestions();
      });
  }

  function restartGame() {
      $(".column").html("");
      team1.x = null;
      team1.y = null;
      team2.x = null;
      team2.y = null;
      initGame()
  }

  function inicializeBackground() {
      $("body").css("background", K_BACKGROUND);
      $("body").css("background-size", "700px 700px");
      $("#start").css("background", K_FLOOR);
      $("#start").css("background-size", "300px 300px");
      $("#end").css("background", K_FLOOR);
      $("#end").css("background-size", "300px 300px");
  }

  function nextTurn() {
      if (team1.isPlaying) {
          team2.isPlaying = true;
          actualTeam = team2;
          team1.isPlaying = false;
          enemyTeam = team1;
      } else {
          team1.isPlaying = true;
          actualTeam = team1;
          team2.isPlaying = false;
          enemyTeam = team2;
      }
      $("#alert").html("<h3>Turn of the team <b>" + actualTeam.name + "</b></h3>")
  }

  function showPossibleQuestions() {
      let x = actualTeam.x === null ? 0 : actualTeam.x + 1;

      if (actualTeam.x === null) {
          x = 0;
      } else {
          x = actualTeam.x + 1;
          $( "#column" + actualTeam.x ).children().removeClass( "pointer" )
      }

      $( "#column"+x ).children().filter(':not([answered])').addClass( "pointer" )
      $( ".column" ).children().filter(':not([answered])').children().addClass( "blackAndWhite" )

      $( "#column"+x ).children().filter(':not([answered])').children().removeClass( "blackAndWhite" )

      if (enemyTeam.x == x) {
          $("div[x='" + enemyTeam.x + "'][y='" + enemyTeam.y + "']").removeClass( "pointer" )
      }
  }

  // ---------------- UTILS -----------------
  function randsort(arr) {
      for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
        return arr;
  }

  $("#fullscreen").click(function() {
      var elem = document.documentElement;
  var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);

  if (isInFullScreen) {
    cancelFullScreen(document);
  } else {
    requestFullScreen(elem);
  }
  return false;
  });

  function cancelFullScreen(el) {
  var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
  if (requestMethod) { // cancel full screen.
    requestMethod.call(el);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
}

function requestFullScreen(el) {
  // Supports most browsers and their versions.
  var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;

  if (requestMethod) { // Native full screen.
    requestMethod.call(el);
  } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
    var wscript = new ActiveXObject("WScript.Shell");
    if (wscript !== null) {
      wscript.SendKeys("{F11}");
    }
  }
  return false
}
})
