$( document ).ready(function() {
	$(function () {
  		$('[data-toggle="tooltip"]').tooltip();
	})
	"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Team = function () {
	function Team(id, name) {
		_classCallCheck(this, Team);

		this._id = id;
		this._name = name;
		this._playing = false;
		this._x = null;
		this._y = null;
	}

	_createClass(Team, [{
		key: "isPlaying",
		set: function set(playing) {
			this._playing = playing;
		},
		get: function get() {
			return this._playing;
		}
	}, {
		key: "x",
		set: function set(x) {
			this._x = x;
		},
		get: function get() {
			return this._x;
		}
	}, {
		key: "y",
		set: function set(y) {
			this._y = y;
		},
		get: function get() {
			return this._y;
		}
	}, {
		key: "name",
		get: function get() {
			return this._name;
		}
	}, {
		key: "id",
		get: function get() {
			return this._id;
		}
	}]);

	return Team;
}();
	// MODAL INFO
	const K_WELCOME_TITLE = "<b>Benvinguts al joc de creuar el riu!</b>";
	const K_CONTENT= "<p>Aquest joc es basa en contestar les maximes preguntes be per poder arribar a l'altra banda del riu!</p><p>L'equip que arribi abans guanyara!</p><p>Els equips participants son:</p>"
	// BG IMAGES
	var K_BACKGROUND = "url('img/backgrounds/pool.jpg') repeat";
	//var K_BACKGROUND = "url('img/backgrounds/lake.jpg') repeat";
	//var K_BACKGROUND = "url('img/backgrounds/grass.jpg') repeat";
	var K_FLOOR = "url('img/floors/wood.jpg') repeat";
	// TEAM IMAGES
	var K_TEAM1_IMAGE = "img/team/inflatable_pink.svg";
	var K_TEAM2_IMAGE = "img/team/inflatable_green.svg";
	//var K_TEAM1_IMAGE = "img/team/inflatable_pink.svg";
	//var K_TEAM2_IMAGE = "img/team/inflatable_green.svg";
	// QUESTIONS
	const K_QUESTION_IMAGE = "img/float.svg";
	const K_QUESTION_CORRECT = "Respuesta correcta!";
	const K_QUESTION_INCORRECT = "Respuesta incorrecta :(";
	// COUNTDOWN
	const K_COUNTDOWN_TIMEOUT = "Se acabo el tiempo!";
	// BUTTONS
	const K_BUTTON_CONTINUE = "<button type='button' id='continue' class='btn btn-outline-secondary' data-dismiss='modal'>Continuar</button>";
	const K_BUTTON_START = "<button type='button' id='startBtn' class='btn btn-outline-primary' data-dismiss='modal'>Start</button>";
	// Definicion de variables
	var defaultTimeLeft = 100;
	var questionData;
	var seconds;
	// Instacia dels jugadors
	var team1 = new Team(1, "lolis team")
	var team2 = new Team(2, "team2")
	var actualTeam;
	var enemyTeam;
	var question;
	var index;
	var x;
	var y;
	var countdownTimer;

	//	QAArray de objetos de preguntas / respuestas
	var QAArray = [ 					{
											question: "Do u like lolis?",
											correctAnswer: "No im not a pederast.",
											answers: ["Yes i do", "Sometimes", "Maybe", "No im not a pederast."],
											time: 20
										},
										{
											question: "Que vas a hacer el 420?",
											correctAnswer: "Smoke a lot",
											answers: ["Smoke a lot", "Nothing", "What is 420?", "Lolis"],
											time: 20
										},
										{
											question: "Bla bla?",
											correctAnswer: "Yes",
											answers: ["Nope", "No", "N", "Yes"],
											time: 20
										},
										{
											question: "u want to die?1",
											correctAnswer: "Yes",
											answers: ["Nope", "No", "N", "Yes"],
											time: 20
										},
									{
										question: "u want to die?2",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?3",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?4",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?5",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?6",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?7",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?8",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?9",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?10",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?11",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?12",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?13",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?14",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?15",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?16",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "u want to die?17",
										correctAnswer: "Yes",
										answers: ["Nope", "No", "N", "Yes"],
										time: 20
									},
									{
										question: "last question?",
										correctAnswer: "yes",
										answers: ["no", "nope", "n", "yes"],
										time: 20
									}
				];

	var columnNumber = 5;
	var questionsOnColumn = ( QAArray.length - 1 ) / columnNumber;

	initGame();

	// Carga del modal al iniciar la pagina
	function showInfoModal() {
		$("#infoModal .modal-title").html(K_WELCOME_TITLE);
		$("#infoModal .modal-body").append(K_CONTENT);
		$("#infoModal .modal-body").append("<img data-toggle='tooltip' data-placement='bottom' title='" + team1.name + "' class='team mx-auto' src='" + K_TEAM1_IMAGE + "'/><span>" + team1.name + "</span>");
		$("#infoModal .modal-body").append("<img data-toggle='tooltip' data-placement='bottom' title='" + team2.name + "' class='team mx-auto' src='" + K_TEAM2_IMAGE + "'/><span>" + team2.name + "</span>");
		$("#infoModal .modal-footer").html(K_BUTTON_START);
		$('#infoModal').modal('show');
	}

	function inicializeQuestions() {
		index = 0;
		// Mostrar preguntes
		for (var x = 0; x < columnNumber; x++) {
			for (var y = 0; y < questionsOnColumn; y++) {
				$("#column" + x ).append("<div x='" + x + "' y='" + y + "' index='" + index + "' data-toggle='modal' data-target='#questionModal' class='question'><img class='questionImage shadow blackAndWhite' src='" + K_QUESTION_IMAGE + "'/></div>");
				index++;
			}
		}
		// Ultima pregunta
		$("#column5").append("<div x='5' y='0' index='" + index++ + "' data-toggle='modal' data-target='#questionModal' class='question'><img class='questionImage shadow blackAndWhite' src='" + K_QUESTION_IMAGE + "'/></div>");
	}

	// Question click event
	$(".question").click(function() {
		// Netejar anterior pregunta
		$(".modal-body").empty();
		$(".modal-footer").empty();

		// Element de la pregunta
		question = this;

		// Posicions i index
		x = parseInt($(question).attr("x"))
		y = parseInt($(question).attr("y"))
		index = parseInt($(question).attr("index"))

		// Recollir les dades de la pregunta
		questionData = QAArray[index];

		// Afegir dades al modal
		$("#questionModal .modal-title").html("<p><b>" + questionData.question + "</b></p>");

		$.each(randsort(questionData.answers), function( index, answer ) {
			$("#questionModal .modal-body").append("<p><button type='button' class='btn btn-outline-primary answer' value='" + answer + "' >" + answer + "</button></p>");
		});

		// Botó cancelar
		$("#questionModal .modal-body").append("<div class='cancel'></div>");

		// Mostrar contador
		seconds = questionData.time
		function secondPassed() {
			$(".modal-footer").html(seconds);
			if (seconds == 0) {
				clearInterval(countdownTimer);
				$("#questionModal .modal-body").html(K_COUNTDOWN_TIMEOUT);
				$("#questionModal .modal-footer").html(K_BUTTON_CONTINUE);
				// CUTRE AF
				$("#continue").click(function() {
					nextTurn();
					showPossibleQuestions();
				})

			} else {
				seconds--;
			}
		}
		countdownTimer = setInterval(secondPassed, 1000);

		// Contestar la pregunta event
		$(".answer").click(function() {
			clearInterval(countdownTimer);
			$("#questionModal .modal-body").empty();

			// --------------------------  REPOSTA CORRECTA ---------------------------------
			if (this.value == questionData.correctAnswer) {
				// FIXME
				if (x == 5) {
					$("#questionModal .modal-title").html("Game over");
					$("#questionModal .modal-body").html("El equipo " + actualTeam.name + " ha ganado la partida.</br>");
					$("#questionModal .modal-body").append("Enhorabuena!");
				} else {
					$("#questionModal .modal-body").html(K_QUESTION_CORRECT);
				}
				// FIXME
				$(question).attr("answered", "true")
			} else {
				$("#questionModal .modal-body").html(K_QUESTION_INCORRECT);
			}
			clearInterval(countdownTimer);
			$("#questionModal .modal-footer").html(K_BUTTON_CONTINUE);

			$("#continue").click(function() {
				moveActualTeam(x, y)
				nextTurn();
				showPossibleQuestions();
			})
		});
	});


	function moveActualTeam(x, y) {
		if (actualTeam.x == null) {
			$("#team" + actualTeam.id).addClass("animated fadeOut")
		} else {
			$("[x='"+ actualTeam.x +"'][y='"+actualTeam.y+"']").addClass('animated fadeOut')
		}

		actualTeam.x = x;
		actualTeam.y = y;


		if (actualTeam.id == 1) {
			// FIXME
			$("#team1").removeClass('fadeIn');
			$("#team1").addClass('fadeOut');
			$(".question").filter("[x='" + x + "'][y='" + y + "']").append("<img data-toggle='tooltip' id='team1' data-placement='left' title='" + team1.name + "' class='teamMove animated fadeIn shadow' src='" + K_TEAM1_IMAGE + "' id='team1'/>")
		} else {
			// FIXME
			$("#team2").removeClass('fadeIn');
			$("#team2").addClass('fadeOut');
			$(".question").filter("[x='" + x + "'][y='" + y + "']").append("<img data-toggle='tooltip' id='team2' data-placement='left' title='" + team2.name + "' class='teamMove animated fadeIn shadow' src='" + K_TEAM2_IMAGE + "' id='team1'/>")
		}

	}

	function inicializeTeams() {
		$("#teams").append("<img data-toggle='tooltip' id='team1' data-placement='left' title='" + team1.name + "' class='team shadow' src='" + K_TEAM1_IMAGE + "'></img>")
		$("#teams").append("<img data-toggle='tooltip' id='team2' data-placement='left' title='" + team2.name + "' class='team shadow' src='" + K_TEAM2_IMAGE + "'></img>")
	}

	function initGame() {
		inicializeBackground();
		showInfoModal();
		inicializeTeams();
		inicializeQuestions();

		$("#startBtn").click(function() {
			let gameOver = false;
			team1.isPlaying = true;
			actualTeam = team1;
			enemyTeam = team2;

			showPossibleQuestions();
		});
	}

	function inicializeBackground() {
		$("body").css("background", K_BACKGROUND);
		$("body").css("background-size", "700px 700px");
		$("#start").css("background", K_FLOOR);
		$("#start").css("background-size", "300px 300px");
		$("#end").css("background", K_FLOOR);
		$("#end").css("background-size", "300px 300px");
	}

	function nextTurn() {
		if (team1.isPlaying) {
			team2.isPlaying = true;
			actualTeam = team2;
			team1.isPlaying = false;
			enemyTeam = team1;
		} else {
			team1.isPlaying = true;
			actualTeam = team1;
			team2.isPlaying = false;
			enemyTeam = team2;
		}
	}

	function showPossibleQuestions() {
		let x = actualTeam.x === null ? 0 : actualTeam.x + 1;

		$( "#column"+x ).children().filter(':not([answered])').addClass( "pointer" )
		$( ".column" ).children().filter(':not([answered])').children().addClass( "blackAndWhite" )
		$( "#column"+x ).children().filter(':not([answered])').children().removeClass( "blackAndWhite" )

		if (enemyTeam.x == x) {
			$("div[x='" + enemyTeam.x + "'][y='" + enemyTeam.y + "']").removeClass( "pointer" )
		}
	}

	// ---------------- UTILS -----------------
	function randsort(arr) {
		for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], arr[j] = x);
	  	return arr;
	}

	$("#fullscreen").click(function() {
		var elem = document.documentElement;
    var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);

    if (isInFullScreen) {
      cancelFullScreen(document);
    } else {
      requestFullScreen(elem);
    }
    return false;
	});

	function cancelFullScreen(el) {
    var requestMethod = el.cancelFullScreen || el.webkitCancelFullScreen || el.mozCancelFullScreen || el.exitFullscreen;
    if (requestMethod) { // cancel full screen.
      requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
      var wscript = new ActiveXObject("WScript.Shell");
      if (wscript !== null) {
        wscript.SendKeys("{F11}");
      }
    }
  }

  function requestFullScreen(el) {
    // Supports most browsers and their versions.
    var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullscreen;

    if (requestMethod) { // Native full screen.
      requestMethod.call(el);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
      var wscript = new ActiveXObject("WScript.Shell");
      if (wscript !== null) {
        wscript.SendKeys("{F11}");
      }
    }
    return false
  }

});
