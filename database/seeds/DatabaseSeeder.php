<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 10)->create()->each(function ($u){
            $u->stats()->save(factory(App\UserStats::class)->make());
        });
        factory(App\Language::class, 4)->create();
        factory(App\Question::class, 100)->create()->each(function ($q) {
            for($i = 1; $i <= 4; $i++) :
                if ($i % 4 == 0) :
                    $q->answers()->save(factory(App\Answer::class)->make([
                        'correct' => 1,
                    ]));
                else :
                    $q->answers()->save(factory(App\Answer::class)->make());
                endif;
                $q->tags()->save(factory(App\QuestionTag::class)->make());
            endfor;
        });  
        factory(App\Game::class, 100)->create()->each(function ($g) {
            for($i = 1; $i <= 4; $i++) :
                $g->tags()->save(factory(App\GameTag::class)->make());
            endfor;
        }); 
        /* 
            achievements -> Crear achievements manualment
            languages -> Crear 4 random languages DONE
            user -> Crear relacionat amb user_stats DONE
            user_stats -> Es crea a l'hora amb user DONE
            questions -> Crear amb id de user random DONE
            answers -> Creem 4 relacionades amb cada pregunta, forcem 1 sigui true si o si DONE
            question_tag -> Es crea relacionada amb cada question DONE
            games -> Es crea anb id d'un usuari aleatori DONE   
            games_tag -> Es crea relacionada amb cada joc DONE
            achievement_user -> Es creen amb els observers de les classes de les accions d'eloquent
        */

    }
}
