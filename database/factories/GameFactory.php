<?php

use Faker\Generator as Faker;

$factory->define(App\Game::class, function (Faker $faker) {
    return [
        //
        'user_id' => App\User::all()->random()->id,
        'titol' => $faker->sentence(),
        'type' => $faker->randomElement(['potato', 'race']),
        'game' => 'camp text amb una array darrays aki.. XD',
        'votes' => $faker->numberBetween(0, 150)
    ];
});
