<?php

use Faker\Generator as Faker;

$factory->define(App\GameTag::class, function (Faker $faker) {
    return [
        //
        'tag' => $faker->word.$faker->word.$faker->word
    ];
});
