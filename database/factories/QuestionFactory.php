<?php

use Faker\Generator as Faker;
use App\User;
use App\Language;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        //
        'question' => $faker->sentence(),
        'votes' => $faker->numberBetween(0, 150),
        'language_id' => Language::all()->random()->id,
        'user_id' => User::all()->random()->id
    ];
});
