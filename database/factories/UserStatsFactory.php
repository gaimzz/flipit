<?php

use Faker\Generator as Faker;

$factory->define(App\UserStats::class, function (Faker $faker) {
    return [
        'q_answered' => $faker->numberBetween(60, 85),
        'q_correct' => $faker->numberBetween(20, 60),
        'q_created' => 0,
        'g_created' => 0
    ];
});
