<?php

use Faker\Generator as Faker;

$factory->define(App\Language::class, function (Faker $faker) {
    return [
        'lang_code' => $faker->languageCode
    ];
});
