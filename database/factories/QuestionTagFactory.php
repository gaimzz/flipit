<?php

use Faker\Generator as Faker;
use App\Question;

$factory->define(App\QuestionTag::class, function (Faker $faker) {
    return [
        //
        'tag' => $faker->word.$faker->word.$faker->word
    ];
});
